# Pipeline to deploy rs account stack to local master accounts.

## S3 Bucket targets
* Gather user id for target S3 bucket for cfn stack deployments.
	"aws s3api list-buckets" using the account that owns the S3
	bucket to find the owner ID.
	
* Update the pipeline script accordingly

