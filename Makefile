REPO ?= rhedcloud-aws-rs-account-cfn
ARTIFACT ?= $(REPO).latest.zip
BB_TEAM ?= rhedcloud

download:
	download_artifact.sh $(REPO) $(ARTIFACT)
	cfn_tool.py compact $(REPO)/$(REPO).json
	mv $(REPO)/$(REPO).compact.json $(REPO).compact.json


.EXPORT_ALL_VARIABLES:
